<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Доступ запрещен!!!</title>
</head>
<style type="text/css">
    h1{
        color: red;
        font-size: 12pt;
        line-height:0.5
    }
    h2{
        font-size: 11pt;
        line-height:0.5
    }
</style>
<body style="text-align: center">
<h1>Вы ввели не верный логин или пороль</h1>
<h2>Попробуйте еще раз</h2>
<%--у Вас осталось ...   попыток--%>
<jsp:include page="WEB-INF/pages/login_password/login_form.jsp"/>
</body>
</html>