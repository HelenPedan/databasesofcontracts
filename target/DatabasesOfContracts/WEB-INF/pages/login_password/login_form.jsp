<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <title>CONTRACTS</title>
    <link href="${pageContext.request.contextPath}/resources/css/styl_form_login.css" rel="stylesheet" type="text/css" />
    <p id="title_header">${message}</p>
    <img src="${pageContext.request.contextPath}/resources/images/ukr-kontur-contracts.jpg" alt=""/>
</head>
<body>
<form method="post" action="<c:url value='/j_spring_security_check'/>">
    <label>
        Логин: <input type="text" name="j_username" placeholder="Введите логин"/>
    </label>
    <br>
    <label>
        Пароль: <input type="password" name="j_password" placeholder="Введите пароль"/>
    </label>
    <br>
    <br>
    <label>
        <input type ="submit" value="Вход"/>
    </label>
</form>
<div id = "foother">
    <img src="${pageContext.request.contextPath}/resources/images/logo_osn.jpg" alt=""/>  &nbsp &nbsp &nbsp &copy; Helen &nbsp JavaПП-11o-1 &nbsp Компьютерная  &nbsp Академия  &nbsp ШАГ &nbsp 2014
</div>
</body>
</html>
