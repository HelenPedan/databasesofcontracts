package com.springapp.security.DB.DAO;

import com.springapp.security.DB.entity.Contract;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate3.HibernateTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public class ContractDAO {
    @Autowired
    HibernateTemplate hibernateTemplate;

    public List<Contract> findAllContract() {
        return hibernateTemplate.find("from Contract");
    }

    public Contract findById(Long id) {
        return  hibernateTemplate.get(Contract.class, id);
    }

    public void save (Contract contract){
        hibernateTemplate.save(contract);
    }

    public void deleteDepartament(Long id){
        Contract contract = hibernateTemplate.get(Contract.class, id);
        if (null != contract){
            hibernateTemplate.delete(contract);
        }
    }

    public void update(Contract contract){
        hibernateTemplate.update(contract);
    }

}
