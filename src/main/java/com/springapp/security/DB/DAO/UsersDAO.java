package com.springapp.security.DB.DAO;

import com.springapp.security.DB.entity.Users;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate3.HibernateTemplate;
import org.springframework.stereotype.Repository;

import javax.annotation.security.RolesAllowed;
import java.util.List;

@Repository
public class UsersDAO {
    @Autowired
    HibernateTemplate hibernateTemplate;

    public List<Users> findAllUsers() {
        return hibernateTemplate.find("from Users");
    }

    public Users findById(Long id) {
        return  hibernateTemplate.get(Users.class, id);
    }

    //дает право изменять пароль только данной роли
    @RolesAllowed({"ROLE_USER","ROLE_ADMIN","ROLE_SUPER_USER"})
    public void save(Users users){
        hibernateTemplate.save(users);
    }

    @RolesAllowed({"ROLE_USER","ROLE_ADMIN","ROLE_SUPER_USER"})
    public void update(Long id){
        Users users = hibernateTemplate.get(Users.class, id);
        if (null != users){
            hibernateTemplate.update(id);
        }
    }

    @RolesAllowed("ROLE_ADMIN")
    public void deleteUsers(Long id){
        Users users = hibernateTemplate.get(Users.class, id);
        if (null != users){
            hibernateTemplate.delete(users);
        }
    }
}

