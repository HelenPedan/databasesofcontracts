package com.springapp.security.DB.DAO;

import com.springapp.security.DB.entity.Access;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate3.HibernateTemplate;
import org.springframework.stereotype.Repository;

import javax.annotation.security.RolesAllowed;
import java.util.List;


@Repository
public class AccessDAO {
    @Autowired
    HibernateTemplate hibernateTemplate;

    public List<Access> findAllAccess() {
        return hibernateTemplate.find("from Access");
    }

    public Access findById(Long id) {
        return  hibernateTemplate.get(Access.class, id);
    }

    @RolesAllowed({"ROLE_USER","ROLE_ADMIN","ROLE_SUPER_USER"})
    public void save(Access access){
        hibernateTemplate.save(access);
    }

    @RolesAllowed({"ROLE_USER","ROLE_ADMIN","ROLE_SUPER_USER"})
    public void update(Long id){
        Access access = hibernateTemplate.get(Access.class, id);
        if (null != access){
            hibernateTemplate.update(id);
        }
    }

    @RolesAllowed("ROLE_ADMIN")
    public void deleteAccess(Long id){
        Access access = hibernateTemplate.get(Access.class, id);
        if (null != access){
            hibernateTemplate.delete(access);
        }
    }
}
