package com.springapp.security.DB.DAO;

import com.springapp.security.DB.entity.Client;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate3.HibernateTemplate;
import org.springframework.stereotype.Repository;
import java.util.List;

@Repository
public class ClientDAO {
    @Autowired
    HibernateTemplate hibernateTemplate;

    public List<Client> findAllClient() {
        return hibernateTemplate.find("from Client");
    }

    public Client findById(Long id) {
        return  hibernateTemplate.get(Client.class, id);
    }

    public void save (Client client){
        hibernateTemplate.save(client);
    }

    public void update(Client client){
        hibernateTemplate.update(client);
    }

    public void deleteSotrudnik(Long id){
        Client client = hibernateTemplate.get(Client.class, id);
        if (null != client){
            hibernateTemplate.delete(client);
        }
    }
}
