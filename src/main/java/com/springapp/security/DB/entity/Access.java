package com.springapp.security.DB.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@Entity
public class Access implements Serializable{

    @Id
    @GeneratedValue
    @Column(name = "id")
    private Long id;

    private String username;
    private String authority;

    @OneToMany(fetch = FetchType.EAGER)
    private Set<Users> usersList = new HashSet<Users>();

    public Access(String username, String authority, Set<Users> usersList) {
        this.username = username;
        this.authority = authority;
        this.usersList = usersList;
    }

    public Access(String username, String authority) {
        this.id = id;
        this.username = username;
        this.authority = authority;
        this.usersList = new HashSet<Users>();
    }

    public Access() {
        this.username = "Undefined";
        this.authority = "Undefined";
        this.usersList = new HashSet<Users>();
    }

    public boolean addUsers(Users users){
        return usersList != null && usersList.add(users);
    }

    public boolean removeUsers(Users users){
        return usersList != null && usersList.remove(users);
    }

    public Long getId() {return id;}

    public void setId(Long id) {this.id = id;}

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getAuthority() {return authority;}

    public void setAuthority(String authority) {this.authority = authority;}

    public Set<Users> getUsersList() {
        return usersList;
    }

    public void setUsersList(Set<Users> usersList) {
        this.usersList = usersList;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Access access = (Access) o;

        if (!authority.equals(access.authority)) return false;
        if (!username.equals(access.username)) return false;
        if (!usersList.equals(access.usersList)) return false;
        return true;
    }

    @Override
    public int hashCode() {
        int result = (username + authority).hashCode();
        result = 31 * result + usersList.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "Access{" +
                ", username='" + username + '\'' +
                ", authority='" + authority + '\'' +
                ", usersList='" + usersList + '\'' +
                '}';
    }
}