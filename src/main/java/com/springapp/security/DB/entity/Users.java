package com.springapp.security.DB.entity;

import javax.persistence.*;
import java.io.Serializable;


@Entity
public class Users implements Serializable {

    @Id
    @GeneratedValue
    @Column(name = "id")
    private Long id;

    private String username;

    private String password;
    private boolean enable;

    public Users(Long id, String username, String password, boolean enable) {
        this.id = id;
        this.username = username;
        this.password = password;
        this.enable = enable;
    }

    public Users(String username, String password, boolean enable) {
        this.username = username;
        this.password = password;
        this.enable = enable;
    }

    public Users() {
        this.username = "Undefined";
        this.password = "Undefined";
        this.enable = true;
    }

    public Long getId() {return id;}

    public void setId(Long id) {this.id = id;}

    public String getUsername(){return username;}

    public void setUsername(String username) {this.username = username;}

    public String getPassword() {return password;}

    public void setPassword(String password) {this.password = password;}

    public boolean isEnable() {return enable;}

    public void setEnable(boolean enable) {this.enable = enable;}

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Users)) return false;

        Users users = (Users) o;

        if (!username.equals(users.username)) return false;
        if (!password.equals(users.password)) return false;
        if (enable != users.enable) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (username + password).hashCode();
        result = 31 * result + (enable ? 0:1);
        return result;
    }

    @Override
    public String toString() {
        return "Users{" +
                "username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", enable='" + enable +
                '}';
    }
}

