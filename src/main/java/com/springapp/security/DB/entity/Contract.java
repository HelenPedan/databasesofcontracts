package com.springapp.security.DB.entity;

import javax.persistence.*;
import java.text.DecimalFormat;
import java.util.Date;


@Entity
public class Contract {

    @Id
    @GeneratedValue
    @Column(name = "contract_id")
    private Long id;

    private String numberContract;
    private Date dataStart;
    private DecimalFormat price;
    private Integer quantity;
    private Date dataEndRent;
    private Date dataEndPrice;


    @OneToOne(mappedBy = "contract")
    public Client client;

    public Contract(Long id, String numberContract, Date dataStart, DecimalFormat price, Integer quantity, Date dataEndRent, Date dataEndPrice) {
        this.id = id;
        this.numberContract = numberContract;
        this.dataStart = dataStart;
        this.price = price;
        this.quantity = quantity;
        this.dataEndRent = dataEndRent;
        this.dataEndPrice = dataEndPrice;
    }

    public Contract(String numberContract, Date dataStart, DecimalFormat price, Integer quantity, Date dataEndRent, Date dataEndPrice) {
        this.numberContract = numberContract;
        this.dataStart = dataStart;
        this.price = price;
        this.quantity = quantity;
        this.dataEndRent = dataEndRent;
        this.dataEndPrice = dataEndPrice;
    }

    public Long getId() {return id;}

    public void setId(Long id) {this.id = id;}

    public String getNumberContract() {return numberContract;}

    public void setNumberContract(String numberContract) {this.numberContract = numberContract;}

    public Date getDataStart() {return dataStart;}

    public void setDataStart(Date dataStart) {this.dataStart = dataStart;}

    public DecimalFormat getPrice() {return price;}

    public void setPrice(DecimalFormat price) {this.price = price;}

    public Integer getQuantity() {return quantity;}

    public void setQuantity(Integer quantity) {this.quantity = quantity;}

    public Date getDataEndRent() {return dataEndRent;}

    public void setDataEndRent(Date dataEndRent) {this.dataEndRent = dataEndRent;}

    public Date getDataEndPrice() {return dataEndPrice;}

    public void setDataEndPrice(Date dataEndPrice) {this.dataEndPrice = dataEndPrice;}

    public Client getClient() {return client;}

    public void setClient(Client client) {this.client = client;}

    public Contract() {
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Contract contract = (Contract) o;

//        if (client != null ? !client.equals(contract.client) : contract.client != null) return false;
        if (dataEndPrice != null ? !dataEndPrice.equals(contract.dataEndPrice) : contract.dataEndPrice != null)
            return false;
        if (dataEndRent != null ? !dataEndRent.equals(contract.dataEndRent) : contract.dataEndRent != null)
            return false;
        if (dataStart != null ? !dataStart.equals(contract.dataStart) : contract.dataStart != null) return false;
//        if (id != null ? !id.equals(contract.id) : contract.id != null) return false;
        if (numberContract != null ? !numberContract.equals(contract.numberContract) : contract.numberContract != null)
            return false;
        if (price != null ? !price.equals(contract.price) : contract.price != null) return false;
        if (quantity != null ? !quantity.equals(contract.quantity) : contract.quantity != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
//        int result = id != null ? id.hashCode() : 0;
        int result = (numberContract != null ? numberContract.hashCode() : 0);
        result = 31 * result + (dataStart != null ? dataStart.hashCode() : 0);
        result = 31 * result + (price != null ? price.hashCode() : 0);
        result = 31 * result + (quantity != null ? quantity.hashCode() : 0);
        result = 31 * result + (dataEndRent != null ? dataEndRent.hashCode() : 0);
        result = 31 * result + (dataEndPrice != null ? dataEndPrice.hashCode() : 0);
        result = 31 * result + (client != null ? client.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Contract{" +
                ", numberContract='" + numberContract + '\'' +
                ", dataStart=" + dataStart +
                ", price=" + price +
                ", quantity=" + quantity +
                ", dataEndRent=" + dataEndRent +
                ", dataEndPrice=" + dataEndPrice +
                '}';
    }
}


