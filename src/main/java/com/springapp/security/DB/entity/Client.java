package com.springapp.security.DB.entity;


import javax.persistence.*;
import java.io.Serializable;
@Entity
public class Client implements Serializable {

    @Id
    @GeneratedValue
    @Column(name = "client_id")
    private Long id;

    private String name;
    private Integer inn;
    private Integer account;


    @OneToOne(cascade = CascadeType.ALL)
    @JoinTable(name = "client_contract",
            joinColumns = @JoinColumn(name = "client_id"),
            inverseJoinColumns = @JoinColumn(name = "contract_id"))
    private Contract contract;

    public Client(Long id, String name, Integer inn, Integer account ) {
        this.id = id;
        this.name = name;
        this.inn = inn;
        this.account = account;
    }

    public Client(String name, Integer inn, Integer account) {
        this.name = name;
        this.inn = inn;
        this.account = account;
    }

    public Client() {
        this.name = "Undefined";
        this.inn = 0;
        this.account = 0;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getINN() {
        return inn;
    }

    public void setINN(Integer inn) {
        this.inn = inn;
    }

    public Integer getAccount() {return  account;}

    public void setAccount(Integer account) {this.account = account;}

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Client client = (Client) o;

        if (!name.equals(client.name)) return false;
        if (inn != client.inn) return false;
        if (account != client.account) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = name.hashCode();
        result = 31 * result + inn + account;
        return result;
    }

    @Override
    public String toString() {
        return "Client{" +
                ", name='" + name + '\'' +
                ", inn='" + inn +
                ", account='" + account +
                '}';
    }
}
