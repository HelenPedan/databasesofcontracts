package com.springapp.security.DB.service.OsnService;

import com.springapp.security.DB.entity.Client;
import com.springapp.security.DB.entity.Contract;
import com.springapp.security.DB.service.DataServise;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.text.DecimalFormat;
import java.util.Date;
import java.util.List;

@Controller
@RequestMapping("/DB/service/OsnService")
public class ContractsService {

    @Autowired
    DataServise dataServise;

    @RequestMapping(value="showClients", method = RequestMethod.GET)
    public String showClients(ModelMap contractsModel) {
        List<Client> clientList = dataServise.getAllClients();
        contractsModel.addAttribute("clientList", clientList);

        return "redirect:showContracts";
    }

    @RequestMapping(value="showContracts", method = RequestMethod.GET)
    public String showContracts(ModelMap contractsModel) {
        List<Contract> contractList = dataServise.getAllContracts();
        contractsModel.addAttribute("contractList", contractList);

        return "client_contract/ekvar_table";
    }


    @RequestMapping(value = "createClientsStart", method = RequestMethod.GET)
    public String createClientStart(ModelMap contractsModel) {

        return "client_contract/ekvar";
    }

    @RequestMapping(value = "addClient", method = RequestMethod.GET)
    public String addClient(
            @RequestParam(value = "clientName", required = false) String clientName,
            @RequestParam(value = "clientINN", required = false) Integer clientINN,
            @RequestParam(value = "clientAccount", required = false) Integer clientAccount,
//                @RequestParam(value = "departamentList", required = false) List<Contract> departamentList,
            ModelMap contractsModel) {

        dataServise.addClient(clientName, clientINN, clientAccount);

        return "redirect:addContract";
    }

    @RequestMapping(value = "addContract", method = RequestMethod.GET)
    public String addContract(
            @RequestParam(value = "contractID", required = false) Long contractID,
            @RequestParam(value = "contractNumberContract", required = false) String contractNumberContract,
            @RequestParam(value = "contractDataStart", required = false) Date contractDataStart,
            @RequestParam(value = "contractPrice", required = false) DecimalFormat contractPrice,
            @RequestParam(value = "contractQuantity", required = false) Integer contractQuantity,
            @RequestParam(value = "contractDataEndRent", required = false) Date contractDataEndRent,
            @RequestParam(value = "contractDataEndPrice", required = false) Date contractDataEndPrice,
            ModelMap contractsModel) {

        dataServise.addContract(contractID, contractNumberContract, contractDataStart, contractPrice, contractQuantity, contractDataEndRent, contractDataEndPrice);
        return "redirect:showClients";
    }

    @RequestMapping("/deleteClient/{id}")
    public String deleteClient(@PathVariable ("id") Long  id,
                                  ModelMap contractsModel){
        dataServise.deleteClient(id);
        return "redirect:deleteContract";
    }

    @RequestMapping("/deleteContract/{id}")
    public String deleteContract(@PathVariable ("id") Long  id,
                                    ModelMap contractsModel){
        dataServise.deleteContract(id);
        return "redirect:showClients";
    }

}
