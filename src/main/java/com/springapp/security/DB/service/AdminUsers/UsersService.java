package com.springapp.security.DB.service.AdminUsers;

import com.springapp.security.DB.entity.Access;
import com.springapp.security.DB.entity.Users;
import com.springapp.security.DB.service.DataServise;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@Controller
@RequestMapping("/DB/service/AdminUsers")
public class UsersService {

    @Autowired
    DataServise dataServise;

    @RequestMapping(value="showAccess", method = RequestMethod.GET)
    public String showAccess(ModelMap userModel) {
        List<Access> accessList = dataServise.getAllAccess();
        userModel.addAttribute("accessList", accessList);

        return "prava_admin/show_users";
    }


    @RequestMapping(value = "createUsersStart", method = RequestMethod.GET)
    public String createUsersStart(ModelMap userModel) {

        return "prava_admin/create_user";
    }

    @RequestMapping(value = "addUsers", method = RequestMethod.GET)
    public String addUsers(
            @RequestParam(value = "usersUsername", required = false) String usersUsername,
            @RequestParam(value = "usersPassword", required = false) String usersPassword,
            @RequestParam(value = "userEnable", required = false) boolean userEnable,
//            @RequestParam(value = "userAccess", required = false) Access userAccess,
            ModelMap userModel) {

        dataServise.addUsers(usersUsername, usersPassword, userEnable);

        return "redirect:addAccess";
    }

    @RequestMapping(value = "addAccess", method = RequestMethod.GET)
    public String addAccess(
            @RequestParam(value = "accessUsername", required = false) String accessUsername,
            @RequestParam(value = "accessAuthority", required = false) String accessAuthority,
            ModelMap userModel) {

        dataServise.addAccess(accessUsername, accessAuthority);
        return "redirect:showUsers";
    }

    @RequestMapping(value = "/updateUsers/{username}", method = RequestMethod.GET)
    public String updateUsers(
            @RequestParam(value = "usersUsername", required = false) String usersUsername,
            @RequestParam(value = "usersPassword", required = false) String usersPassword,
            @RequestParam(value = "userEnable", required = false) boolean userEnable,
//            @RequestParam(value = "userAccess", required = false) Access userAccess,
            ModelMap userModel) {

        dataServise.updateUsers(usersUsername, usersPassword, userEnable);

        return "redirect:updateAccess";
    }

    @RequestMapping(value = "/updateAccess/{username}", method = RequestMethod.GET)
    public String updateAccess(
            @RequestParam(value = "accessUsername", required = false) String accessUsername,
            @RequestParam(value = "accessAuthority", required = false) String accessAuthority,
            ModelMap userModel) {

        dataServise.addAccess(accessUsername, accessAuthority);
        return "redirect:showUsers";
    }

//    @RequestMapping("/updateUsers/{username}")
//    public String updateUsers(@PathVariable("username") String username, String password, boolean enable,
//                                  ModelMap contractsModel){
//        dataServise.updateUsers(username, password, enable);
//        return "redirect:updateAccess";
//    }
//
//    @RequestMapping("/updateAccess/{username}")
//    public String updateAccess(@PathVariable ("username") String username, String authority,
//                                    ModelMap contractsModel){
//        dataServise.updateAccess(username, authority);
//        return "redirect:showUsers";
//    }

    @RequestMapping("/deleteUsers/{id}")
    public String deleteUsers(@PathVariable ("id") Long  id,
                              ModelMap userModel){
        dataServise.deleteUsers(id);
        return "redirect:deleteAccess";
    }

    @RequestMapping("/deleteAccess/{id}")
    public String deleteAccess(@PathVariable ("id") Long  id,
                               ModelMap userModel){
        dataServise.deleteAccess(id);
        return "redirect:showUsers";
    }

}
