package com.springapp.security.DB.service;

import com.springapp.security.DB.DAO.AccessDAO;
import com.springapp.security.DB.DAO.ContractDAO;
import com.springapp.security.DB.DAO.ClientDAO;
import com.springapp.security.DB.DAO.UsersDAO;
import com.springapp.security.DB.entity.Access;
import com.springapp.security.DB.entity.Client;
import com.springapp.security.DB.entity.Contract;
import com.springapp.security.DB.entity.Users;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.text.DecimalFormat;
import java.util.Date;
import java.util.List;

@Service
public class DataServise {

    @Autowired
    private ContractDAO contractDAO;

    @Autowired
    private ClientDAO clientDAO;

    @Autowired
    private UsersDAO usersDAO;

    @Autowired
    private AccessDAO accessDAO;

    public DataServise() {
    }

    public List<Client> getAllClients() {
        return clientDAO.findAllClient();
    }

    public List<Contract> getAllContracts() {
        return contractDAO.findAllContract();
    }

    public List<Users> getAllUsers(){
        return usersDAO.findAllUsers();
    }

    public List<Access> getAllAccess(){
        return accessDAO.findAllAccess();
    }

    public void addClient(String name, Integer inn, Integer account) {
        Client client = new Client(name, inn, account);
        clientDAO.save(client);
    }

    public void addContract(Long id, String numberContract, Date dataStart, DecimalFormat price, Integer quantity,Date dataEndRent, Date dataEndPrice) {
        Contract contract = new Contract(id, numberContract, dataStart, price, quantity, dataEndRent, dataEndPrice);
        contractDAO.save(contract);
    }

    public void addUsers(String username, String password, boolean enable){
        Users users = new Users(username, password, enable);
        usersDAO.save(users);
    }

    public void addAccess(String username, String authority){
        Access access = new Access(username, authority);
        accessDAO.save(access);
    }

    public  void saveClient(Client client){
        clientDAO.save(client);
    }

    public void saveContract(Contract contract){
        contractDAO.save(contract);
    }

    public void saveUsers(Users users){
        usersDAO.save(users);
    }

    public void saveAccess(Access access){
        accessDAO.save(access);
    }

    public void updateUsers(String username, String password, boolean enable){
        Users users = new Users(username, password, enable);
        usersDAO.save(users);
    }

    public void updateAccess(String username, String authority){
        Access access = new Access(username, authority);
        accessDAO.save(access);
    }

        @Transactional
    public void deleteClient(Long id){
        clientDAO.deleteSotrudnik(id);
    }

        @Transactional
    public void deleteContract(Long id){
        contractDAO.deleteDepartament(id);
    }

        @Transactional
    public void deleteUsers(Long id){
        usersDAO.deleteUsers(id);
    }

        @Transactional
    public void deleteAccess(Long id){
        accessDAO.deleteAccess(id);
    }


}
