package com.springapp.security.Controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("/")
public class LoginPagesController {
    @RequestMapping(method = RequestMethod.GET)
    public String mainPage(ModelMap newModel){
        newModel.addAttribute("message", "Только зарегистрированный пользователь может работать с базой!!!");
        return "login_password/login_passw";
    }
}



