<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<html>
<head>
    <title></title>
</head>
<body>
<sec:authorize access="hasAnyRole('ROLE_USER','ROLE_SUPER_USER')">
    <c:redirect url="/DB/service/OsnService/showClients" />
</sec:authorize>
<sec:authorize access="hasRole('ROLE_ADMIN')">
    <c:redirect url="/DB/service/AdminUsers/showAccess" />
</sec:authorize>
</body>
</html>
