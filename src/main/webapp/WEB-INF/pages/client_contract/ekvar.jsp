<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>
<head>
    <title></title>
</head>
<body>

<form action="${pageContext.request.contextPath}/DB/service/OsnService/addClient">
    <table>

        <tr>
            <td><input name="clientName" type="text">Введите название юридического лица</td>
        </tr>
        <tr>
            <td><input name="clientINN" type="number">Введите налоговый номер</td>
        </tr>
        <tr>
            <td><input name="clientAccount" type="number">Введите счет</td>
        </tr>

        <form action="${pageContext.request.contextPath}/DB/service/OsnService/addContract">
            <table>
                <tr>
                    <td><input name="contractNumberContract" type="text">Введите номер контракта</td>
                </tr>
                <tr>
                    <td><input name="contractDataStart" type="date">Введите дату контракта</td>
                </tr>
                <tr>
                    <td><input name="contractPrice" type="number" >Введите цену ПОС-терминала</td>
                </tr>
                <tr>
                    <td><input name="contractQuantity" type="number">Введите количество ПОС-терминалов</td>
                </tr>
                <tr>
                    <td><input name="contractDataEndRent" type="date" pattern="dd-MM-yyyy">Введите окончание срока оренды</td>
                </tr>
                <tr>
                    <td><input name="contractDataEndPrice" type="date">Введите дату окончания оплаты аванса</td>
                </tr>
            </table>

            <input type="submit" value="Создать">
        </form>
    </table>
</form>
</body>
</html>