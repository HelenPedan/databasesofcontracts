<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="cc" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<html>
<head>
    <title>Предприятие</title>
</head>

<script>
    function radioCheck(){
        var a = document.getElementsByName('object'), s=false
        for (var i = 0; i < a.length; i++)  {
            if  (a[i].checked) {
                s = true;
                break;
            }
        }
        if (s) {
            href="${pageContext.request.contextPath}/deleteClient/${client.getId()}"
        } else {
            alert('Выберите строку!!');
        }
    }
</script>

<style type="text/css">
    h1{
        font-size: 14pt;
    }
</style>


<body>

<h1>Действующая база клиентов</h1>
<form action="${pageContext.request.contextPath}/DB/service/OsnService/createClientsStart">
    <table border="1">
        <tr>
            <th></th>
            <th>номер</th>
            <th>наименование</th>
            <th>ЭДРПОУ</th>
            <th>счет</th>
            <th>номер <br/> контракта <br/> шт.</th>
            <th>дата <br/> контракта <br/> грн.</th>
            <th>цена <br/> ПОС-терминала</th>
            <th>количество <br/> ПОС-терминалов</th>
            <th>дата окончание <br/> догора оренды</th>
            <th>дата окончания <br/> оплаты</th>
        </tr>

        <c:forEach items="${clientList}" var="client">
            <cc:forEach items="${client.contractList}" var="contract">
                <tr>
                    <td><input type="radio" name="object"/></td>
                    <td>${client.getId()}</td>
                    <td>${client.getName()}</td>
                    <td>${client.getINN()}</td>
                    <td>${client.getAccount()}</td>
                    <td>${contract.getNumberContract()}</td>
                    <td>${contract.getDataStart()}</td>
                    <td>${contract.getPrice()}</td>
                    <td>${contract.getQuantity()}</td>
                    <td>${contract.getDataEndRent()}</td>
                    <td>${contract.getDataEndPrice()}</td>
                </tr>
            </cc:forEach>
        </c:forEach>

    </table>


    <label>
        <input type="submit" value="Созадть"/>
    </label>
    <label>
        <input type="button" id="del" name="dell" value="Удалить" onclick="radioCheck()"/>
    </label>
    <label>
        <input type="button" id="update" name="update" value="Редактировать" onclick="radioCheck()"/>
    </label>
</form >

</body>
</html>