<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="cc" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <title>Users</title>
    <link href="${pageContext.request.contextPath}/resources/css/styl_create_users.css" rel="stylesheet" type="text/css" />
    <script src="http://code.jquery.com/jquery-latest.js"></script>
    <script type='text/javascript' src='//code.jquery.com/jquery-1.9.1.js'></script>
    <script src="${pageContext.request.contextPath}/resources/cod_script/users_tabl.js" type="text/javascript"></script>

</head>
<body>
<div id="date"></div>
<div id="day_one"></div>
<div id="time"></div>
<h1>Действующая база прав доступа к операциям сотрудников</h1>
    <center><form action="${pageContext.request.contextPath}/DB/service/AdminUsers/createUsersStart">
       <center><table class="show_users_role" border="2px">
            <tr>
                <th style="width: 2px"></th>
                <th>№ записи</th>
                <th>логин</th>
                <th>активность юзера</th>
                <th>роль юзера</th>
            </tr>
                     <c:forEach items="${accessList}" var="access" >
                         <cc:forEach items="${access.usersList}" var="users" >
                            <tr>
                                 <td style="width: 2px"><input type="radio" name="object"/></td>
                                 <td>${users.getId()}</td>
                                 <td>${users.getUsername()}</td>
                                 <td>${users.isEnable()}</td>
                                 <td>${access.getAuthority()}</td>
                            </tr>
                         </cc:forEach>
                     </c:forEach>
       </table></center>
        <label>
            <input type="submit" value="Созадть"/>
        </label>
        <label>
            <input type="button" id="update" name="update" value="Редактировать" onclick="radioCheck()"/>
        </label>
        <label>
            <input type="button" id="write" name="write" value="Сформировать отчет" onsubmit='return writeUsers(this);'/>
        </label>
    </form ></center>
<script type="text/javascript">clock();</script>
</body>
</html>
