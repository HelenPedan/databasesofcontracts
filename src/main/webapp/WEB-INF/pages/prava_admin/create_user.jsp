<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <title>Users</title>

    <link href="${pageContext.request.contextPath}/resources/css/styl_create_users.css" rel="stylesheet" type="text/css" />
    <script src="${pageContext.request.contextPath}/resources/cod_script/users_tabl.js" type="text/javascript"></script>
</head>
<body>
<div id="date"></div>
<div id="day_one"></div>
<div id="time"></div>
<center>
    <h1>Регистрация нового пользователя</h1>
    <form action="${pageContext.request.contextPath}/DB/service/AdminUsers/addUsers" onsubmit='return check(this);'>
        <center><table border="2px">
            <tr>
                <td><input name="usersUsername" type="text" id="log" style="width:200px"  maxlength="20" required>&nbsp &nbsp &nbsp Введите логин</td>
            </tr>
            <tr>
                <td><input name="usersPassword" type="password" id="one" style="width:200px" maxlength="20" required>&nbsp &nbsp &nbsp Введите пароль</td>
            </tr>
            <tr>
                <td><input name="usersPassword1" type="password" id="two" style="width:200px" maxlength="20" required>&nbsp &nbsp &nbsp Повторите пароль</td>
            </tr>
            <tr>
                <td><select name="usersEnable" id="enable_aktiv" style="width:250px">
                    <option value="0">Выберите статус активности 1/0</option>
                    <option value="1">1</option>
                    <option value="2">0</option>
                </select>
                </td>
            </tr>

            <form action="${pageContext.request.contextPath}/DB/service/AdminUsers/addAccess">
                <table border="2px">
                    <tr>
                        <td><select name="accessAuthority" id="role_aktiv" style="width:250px">
                            <option value="0">Выберите роль юзера</option>
                            <option value="1">ROLE_ADMIN</option>
                            <option value="2">ROLE_USER</option>
                            <option value="3">ROLE_SUPER_USER</option>
                        </select>
                        </td>
                    </tr>
                </table>
                <input type="submit" value="Сохранить">
            </form>
        </table>
        </center>
    </form>
</center>
<script type="text/javascript">clock();</script>
</body>
</html>